import React, { Component } from "react";
import { Easing, Animated } from "react-native";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from "./src/Screens/SplashScreen";
import RegistrationScreen from "./src/Screens/RegistrationScreen";
import LoginScreen from "./src/Screens/LoginScreen";
import HomeScreen from "./src/Screens/HomeScreen";
import DetailScreen from "./src/Screens/DetailScreen";
import OTPScreen from "./src/Screens/OTPScreen";


const RootStack = createStackNavigator(
  {
    SplashScreen: { screen: SplashScreen },
    LoginScreen: { screen: LoginScreen, navigationOptions:{
      header: false
    }},
    RegistrationScreen: { screen: RegistrationScreen, navigationOptions:{
      header: false
    }},
    OTPScreen: { screen: OTPScreen, navigationOptions:{
      header: false,   headerLeft: null
    }},
    HomeScreen: { screen: HomeScreen},
    DetailScreen: { screen: DetailScreen},

  
  },
  {
    initialRouteName: "SplashScreen",
    transitionConfig: () => ({
      transitionSpec: {
        duration: 300,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing
      },
      screenInterpolator: sceneProps => {
        const { layout, position, scene } = sceneProps;
        const { index } = scene;

        const width = layout.initWidth;
        const translateX = position.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [width, 0, 0]
        });

        const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1]
        });

        return { opacity, transform: [{ translateX: translateX }] };
      }
    })
  }
);

const App = createAppContainer(RootStack);

export default App;