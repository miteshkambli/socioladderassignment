import React, { Component } from "react";
import { Dimensions, StyleSheet, Image, View, Platform } from "react-native";
import { StackActions, NavigationActions } from "react-navigation";
import images from "../src/res/images";

class SplashScreen extends Component {
  static navigationOptions = {
    header: null
  };

  getData = async () => {
    try {
        this.navigateAfterFinish("RegistrationScreen");
    } catch (e) {
      // error reading value
    }
  };
  //Splash with wait for 3 seconds
  UNSAFE_componentWillMount() {
    setTimeout(() => {
      this.getData();
    }, 3000);
  }

  navigateAfterFinish = screen => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: screen })]
    });
    this.props.navigation.dispatch(resetAction);
  };

  render() {
    return (
        <View style={styles.container}>
        <Image
            style={styles.logo}
            source={images.splash}
        />
        </View>
    );
  }
}

const win = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
      flex:1 ,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  logo: {
    width: 200,
    height: 200,
    alignItems: 'center'
  },
});

export default SplashScreen;

