//This is an example code for SectionList//
import React, { Component } from 'react';
//import react in our code. 
import { StyleSheet , View , SectionList , Text , Platform , Alert, Image, TouchableOpacity} from 'react-native';
//import all the components we are going to use. 
import images from './res/images'
export default class App extends React.Component {

    constructor(props) {
        //constructor to set default state
        super(props);
        this.state = {
        };
      }
  GetSectionListItem = item => {
    //Function for click on an item
    Alert.alert(item);
  };
  FlatListItemSeparator = () => {
    return (
      //Item Separator
      <View style={{height: 0.5, width: '100%', backgroundColor: '#C8C8C8'}}/>
    );
  };
  
  render() {
    const { navigate } = this.props.navigation;

    var A = [{'id':'1','value':'Ds Kitchen The Food Hub','image': images.food, 'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, {'id':'2','value':'Cafe GUPSHUP','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'},
             {'id':'3','value':'Gossip Cafe','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}];
    var B = [{'id':'4','value':'A B Hair Studio','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, {'id':'5','value':'Sanjanas Beauty Parlour','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, 
             {'id':'6','value':'The Pulse','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, {'id':'7','value':'Live On 2 Salon','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, 
             {'id':'8','value':'Live On 2 Salon','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, {'id':'9','value':'A B Hair Studio','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, 
             {'id':'10','value':'Live On 2 Salon','image': images.food,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'},];
    var C = [{'id':'11','value':'Orchid Beauty Salon & Spa','image': images.spa,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, {'id':'12','value':'Blossom Beauty Salon And Spa','image': images.spa,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, 
             {'id':'13','value':'Orchid Beauty Salon & Spa','image': images.spa,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}, {'id':'14','value':'Blossom Beauty Salon And Spa','image': images.spa,'address': 'Shop No 1,2,3,4,5, N.G Galaxy Mari Gold Road,Near Mayor Bunglow Mira Bhayandar Road, Kanakia Rd, Maharashtra 401107','phoneno':'1234567890','timings':'Opens 10AM'}];
    return (
      <View>
        <SectionList
          ItemSeparatorComponent={this.FlatListItemSeparator}
          sections={[
            { title: 'Food', data: A },
            { title: 'Salon', data: B },
            { title: 'spa', data: C },
          ]}
          renderSectionHeader={({ section }) => (
            <Text style={styles.SectionHeaderStyle}> {section.title} </Text>
          )}
          renderItem={({ item }) => (
            // Single Comes here which will be repeatative for the FlatListItems
            <TouchableOpacity style={{flexDirection: 'row'}}  onPress={() =>
               { navigate('DetailScreen', {
                NAME: item.value, IMAGE:item.image,ADDRESS: item.address, PHONENO: item.phoneno, TIMING: item.timings
              })}}>
            <Image style={{width:100, height:100}} source={item.image}/>
           <View style={{flexDirection: 'column'}}>
           <Text
              style={styles.SectionListItemStyle}>
              {item.value}
            </Text>
            <Text
              style={styles.SubSectionListItemStyle}>
              10% Discount
            </Text>
           </View>
           </TouchableOpacity>

                        
          )}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  SectionHeaderStyle: {
    backgroundColor: '#CDDC89',
    fontSize: 20,
    padding: 5,
    color: '#fff',
  },
  SectionListItemStyle: {
    fontSize: 15,
    padding: 15,
    color: '#000',
    backgroundColor: '#F5F5F5',
  },
  SubSectionListItemStyle: {
    fontSize: 15,
    color: '#000',
    padding: 15,
    backgroundColor: '#F5F5F5',
  },
});