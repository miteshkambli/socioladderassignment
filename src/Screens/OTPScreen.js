import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity
} from "react-native";
import images from "../src/res/images";
import SMSVerifyCode from 'react-native-sms-verifycode'

export default class OTPScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    navigateAfterFinish = screen => {
        this.props.navigation.navigate(screen);
    };
    onInputCompleted = (text) => {
            this.navigateAfterFinish('HomeScreen')

    }
    render() {

        return (
            <View style={styles.container}>
                <Image style={styles.logo}
                    source={images.splash}></Image>
                <Text style={styles.title}>
                    OTP VERIFICATION
          </Text>
                <SMSVerifyCode
                    ref={ref => (this.verifycode = ref)}

                    verifyCodeLength={6}
                    containerPaddingVertical={10}
                    containerPaddingHorizontal={50}
                    codeViewBorderColor="#000000"
                    focusedCodeViewBorderColor="#0000FF"
                    codeViewBorderWidth={3}
                    codeViewBorderRadius={16}
                    codeFontSize={26}
                />
                <TouchableOpacity
                    style={styles.otpScreenButton}
                    onPress={this.onInputCompleted}
                    underlayColor='#fff'>
                    <Text style={styles.otpText}>VERIFY OTP</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        alignSelf: 'center',
        marginTop: 20
    },
    logo: {
        width: 120,
        height: 120,
        marginTop: 30,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    MainContainer: {
        flex: 1,
        margin: 35,
    },
    TextInput: {
        width: '100%',
        height: 50,
        paddingLeft: 5,
        borderWidth: 1,
        marginTop: 15,
        paddingLeft: 20,
        borderColor: '#606070',
        borderRadius: 20
    },
    otpScreenButton: {
        marginTop: 30,
        paddingTop: 10,
        margin: 20,
        paddingBottom: 10,
        backgroundColor: 'yellow',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    otpText: {
        color: '#000000',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        fontWeight: 'bold'
    },
    row: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center'
    },
    loginScreenButton: {
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: 'yellow',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    loginText: {
        color: '#000000',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        fontWeight: 'bold'
    },
})

