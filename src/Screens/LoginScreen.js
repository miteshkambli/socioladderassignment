import React from 'react'
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image
} from "react-native";
import images from "../src/res/images";

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TextInputName: '',
      TextInputEmail: '',
    };
  }
  CheckTextInput = () => {
    //Handler for the Submit onPress
    if (this.state.TextInputName != '') {
      //Check for the Name TextInput
      this.navigateAfterFinish("OTPScreen")
    } else {
      alert('Please  Mobile Number');
    }
  };
  navigateAfterFinish = screen => {
    this.props.navigation.navigate(screen);
  };
  render() {

    return (
      <View style={styles.container}>
        <Image style={styles.logo}
          source={images.splash}></Image>
          <Text style={styles.title}>
           SIGN IN
          </Text>
        <View style={styles.MainContainer}>
          <TextInput
            placeholder="Enter Mobile Number"
            onChangeText={TextInputName => this.setState({ TextInputName })}
            underlineColorAndroid="transparent"
            keyboardType='phone-pad'
            maxLength={10}
            style={styles.TextInput}
          />
         <TouchableOpacity
                style={styles.loginScreenButton}
                onPress={this.CheckTextInput}
                underlayColor='#fff'>
                <Text style={styles.loginText}>Login</Text>
              </TouchableOpacity>
              <View style={styles.row}>
          <Text style={{fontSize: 18}}>Already have an account?  </Text>
          <Text style={{color: 'blue', fontSize: 18}} onPress={() =>this.navigateAfterFinish("RegistrationScreen")}>REGISTER</Text>
        </View>         
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center',
    marginTop: 20
  },
  logo: {
    width: 120,
    height: 120,
    marginTop: 30,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  MainContainer: {
    flex: 1,
    margin: 35,
  },
  TextInput: {
    width: '100%',
    height: 50,
    paddingLeft: 5,
    borderWidth: 1,
    marginTop: 15,
    paddingLeft: 20,
    borderColor: '#606070',
    borderRadius: 20
  },
  loginScreenButton: {
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'yellow',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText: {
    color: '#000000',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    fontWeight: 'bold'
  },
  row: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center'
  }
})

