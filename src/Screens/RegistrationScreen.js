import React from 'react'
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Alert,
  Button,
  Image,
  KeyboardAvoidingView
} from "react-native";
import images from "../src/res/images";
import strings from "../src/res/strings";

export default class RegistrationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TextInputFirstName: '',
      TextInputLastName: '',
      TextInputEmailId: '',
      TextInputPhoneNumber: '',
    };
  }
  CheckTextInput = () => {
    //Handler for the Submit onPress
    if (this.state.TextInputFirstName != '') {
      //Check for the Name TextInput
      if (this.state.TextInputLastName != '') {
        if (this.state.TextInputEmailId != '') {
          if (this.state.TextInputPhoneNumber != '') {
            this.navigateAfterFinish('OTPScreen')
          } else {
            alert('Please Enter Valid Mobile Number');
          }
        } else {
          alert('Please Enter Email ID');
        }
      } else {
        alert('Please Enter Last name');
      }
    } else {
      alert('Please Enter First Name');
    }
  };
  navigateAfterFinish = screen => {
    this.props.navigation.navigate(screen);
  };
  render() {

    return (
      <View style={styles.container}>
        <ScrollView>
          <Image style={styles.logo}
            source={images.splash}></Image>
          <Text style={styles.title}>
            {strings.registration.registrationTitle1}
          </Text>
          <View style={styles.MainContainer}>
            <TextInput
              placeholder={strings.registration.firstName}
              onChangeText={TextInputFirstName => this.setState({ TextInputFirstName })}
              underlineColorAndroid="transparent"
              keyboardType='default'
              maxLength={50}
              returnKeyType='next'
              style={styles.TextInput}
            />
            <TextInput
              placeholder={strings.registration.lastName}
              onChangeText={TextInputLastName => this.setState({ TextInputLastName })}
              underlineColorAndroid="transparent"
              keyboardType='default'
              maxLength={50}
              returnKeyType='next'
              style={styles.TextInput}
            />
            <TextInput
              placeholder={strings.registration.emailId}
              onChangeText={TextInputEmailId => this.setState({ TextInputEmailId })}
              underlineColorAndroid="transparent"
              keyboardType='email-address'
              maxLength={50}
              returnKeyType='next'
              style={styles.TextInput}
            />
            <TextInput
              placeholder={strings.registration.mobileNumber}
              onChangeText={TextInputPhoneNumber => this.setState({ TextInputPhoneNumber })}
              underlineColorAndroid="transparent"
              keyboardType='phone-pad'
              maxLength={10}
              returnKeyType='done'
              style={styles.TextInput}
            />

            <View style={{ marginTop: 15 }}>
              <TouchableOpacity
                style={styles.loginScreenButton}
                onPress={this.CheckTextInput}
                underlayColor='#fff'>
                <Text style={styles.loginText}>Login</Text>
              </TouchableOpacity>
              <View style={styles.row}>
          <Text style={{fontSize: 18}}>Already have an account?  </Text>
          <Text style={{color: 'blue', fontSize: 18}} onPress={() =>this.navigateAfterFinish("LoginScreen")}>LOGIN</Text>
        </View>              
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center',
    marginTop: 20
  },
  logo: {
    width: 120,
    height: 120,
    marginTop: 30,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  MainContainer: {
    flex: 1,
    margin: 20,
  },
  TextInput: {
    width: '100%',
    height: 50,
    paddingLeft: 5,
    borderWidth: 1,
    marginTop: 15,
    paddingLeft: 20,
    borderColor: '#606070',
    borderRadius: 20
  },
  loginScreenButton: {
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'yellow',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText: {
    color: '#000000',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    fontWeight: 'bold'
  },
  row: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center'
  }

})

