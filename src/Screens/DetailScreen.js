// Second screen which we will use to get the data
import React, { Component } from 'react';
//import react in our code.
import { Dimensions,StyleSheet, View, Text, Image } from 'react-native';
import images from './res/images';
//import all the components we are going to use.

export default class DetailScreen extends Component {
  static navigationOptions = {
    //Setting the header of the screen
    title: 'Detail Screen',
  };
  render() {
    const { navigate } = this.props.navigation;
    const win = Dimensions.get("window");

    return (
      //View to hold our multiple components
      <View style={styles.container}>
          <Image style={{width: win.width, height: 250}} source={this.props.navigation.state.params.IMAGE}></Image>
          <Text style={styles.TextStyle}>
          {this.props.navigation.state.params.NAME}        </Text>
        <View>
            <View style={{flexDirection: 'row'}}> 
            <Text style={styles.tableView}>About the place :</Text>
            <Text style={styles.tableView}>{this.props.navigation.state.params.NAME}</Text>
            </View>
        </View>
        <View>
            <View style={{flexDirection: 'row'}}> 
            <Text style={styles.tableView}>Address :</Text>
            <Text style={styles.tableView}>{this.props.navigation.state.params.ADDRESS}</Text>
            </View>
        </View>
        <View>
            <View style={{flexDirection: 'row'}}> 
            <Text style={styles.tableView}>Phone no :</Text>
            <Text style={styles.tableView}>{this.props.navigation.state.params.PHONENO}</Text>
            </View>
        </View>
        <View>
            <View style={{flexDirection: 'row'}}> 
            <Text style={styles.tableView}>Timings :</Text>
            <Text style={styles.tableView}>{this.props.navigation.state.params.TIMING}</Text>
            </View>
        </View>
       
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
   
  },
  TextStyle: {
    fontSize: 23,
    textAlign: 'center',
    color: 'black',
    padding: 10,
    fontWeight: 'bold'
  },
  tableView:{flex:1,borderWidth: 1,padding: 10
  }
});