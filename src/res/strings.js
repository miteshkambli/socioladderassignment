const strings = {
  registration: {
    registrationTitle1: "SIGN UP",
    firstName: "Enter First Name*",
    lastName: "Enter Last Name*",
    emailId: "Enter Email Id*",
    mobileNumber: "Enter Phone Number"
  }
};

export default strings;
